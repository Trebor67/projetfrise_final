package vue;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import controleur.Controleur;

public class FenetreMere extends JFrame implements ActionListener{
	
	
	
	public FenetreMere(String titre) {
		
		super(titre);
		Panel contentPane = new Panel();
		this.setContentPane(contentPane);
		contentPane.setBackground(new Color(25,45,21));
		contentPane.setOpaque(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(700,400);setVisible(true);setLocation(200,300);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setLayout(new FlowLayout(FlowLayout.LEFT));
		String []nomMenu = {"Cr�ation Frise","Ajout Evenement","Affichage","Quitter"};
		JMenuItem []menuItems = new JMenuItem[4];
		
		for (int i = 0 ; i < 4 ; i++) {
			menuItems[i] = new JMenuItem(nomMenu[i]);
			menuBar.add(menuItems[i]);
			menuItems[i].setActionCommand(nomMenu[i]);
			menuItems[i].addActionListener(this);
			menuItems[i].addActionListener(contentPane);
		}
		KeyStroke ctrlXKeyStroke = KeyStroke.getKeyStroke("control C");
		menuItems[0].setAccelerator(ctrlXKeyStroke);
		menuItems[1].setAccelerator(KeyStroke.getKeyStroke("control E"));
		menuItems[2].setAccelerator(KeyStroke.getKeyStroke("control A"));
		menuItems[3].setAccelerator(KeyStroke.getKeyStroke("control Q"));
		this.setJMenuBar(menuBar);

		
	}
	
	public static void main(String []args) {
		FenetreMere f = new FenetreMere("Frise Chronologique");
		
	}

	public void actionPerformed(ActionEvent parEvt) {
		if(parEvt.getActionCommand().equals("Quitter")) {
			this.dispose();
		}
		else if(parEvt.getActionCommand().equals("Cr�ation Frise")) {
			setSize(700,400);
		}
		else if(parEvt.getActionCommand().equals("Ajout Evenement")) {
			setSize(700,650);
		}
		else if(parEvt.getActionCommand().equals("Affichage")) {
			setSize(1200,500);
		}
		
	}
	
}
