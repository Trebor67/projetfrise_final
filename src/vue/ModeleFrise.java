package vue;

import java.util.HashMap;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;

import modele.Classeur;
import modele.Evenement;

public class ModeleFrise extends DefaultTableModel {
	
	private Classeur classeur;
	
	public ModeleFrise(Classeur c) {
		
			classeur = c;
			this.setColumnCount(classeur.getNombreAnnee());
			this.setRowCount(4);
			String id[] = new String[classeur.getNombreAnnee() + 1]; 
			
			for(int i = 0; i <= classeur.getNombreAnnee();i++) {
				if (i%classeur.getPeriode() == 0) {
					id[i] = String.valueOf(i + classeur.getAnneeDebut());
				}
				else {
					id[i] = "";
				}
								
			}
			this.setColumnIdentifiers(id);
			
			TreeMap<Integer,TreeSet<Evenement>> hash = classeur.getClasseur();
			System.out.println(hash);
			for (Integer a : hash.keySet()) {
				TreeSet<Evenement> tsEvt = hash.get(a);
				
				for(Evenement evt : tsEvt) {
					this.setValueAt(evt,-(evt.getImportance() - 4), evt.getDate().getAnnee() - classeur.getAnneeDebut());
					
				}
			}
			
		}
		
	
	public boolean isCellEditable(int indiceLigne, int indiceColonne)
	{
		return false;
	}
	public Class getColumnClass(int parNum)
	{
		return Integer.class;
	}
	
	/**
	 * methode qui parcours tous les �l�ments pr�sent dans la jtable. S'il trouve une valeur, il va recuperer l'image associ� et l'ins�rer dans le tableau 
	 * � double entr�e, sinon il ins�re null
	 * @return Un tableau � double entr�e de type ImageIcon
	 */
	
	public ImageIcon[][] whereAreValues() {
		ImageIcon a[][] = new ImageIcon[4][classeur.getNombreAnnee() + 1];
		
		for(int i = 0 ; i < 4 ; i++) {
			for(int j = 0 ; j < classeur.getNombreAnnee() + 1 ; j++) {
				if(this.getValueAt(i, j) != null) {
					//System.out.println("trouv� l"+i +" c"+j);
					Evenement e = (Evenement) this.getValueAt(i, j);
					a[i][j] = e.getImage();
				}
				else
					a[i][j] = null;
			}
		}
		return a;
	}

}
