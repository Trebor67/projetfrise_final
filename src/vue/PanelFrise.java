package vue;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BoundedRangeModel;
import javax.swing.CellRendererPane;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.ChangeListener;

import controleur.Controleur;
import vue.CellRenderer;
import modele.Classeur;

public class PanelFrise extends JPanel implements MouseListener{

	private Classeur classeur;
	private JTable frise;
	private ModeleFrise model;
	private JScrollPane scrollPane;
	
	public PanelFrise(Classeur c) {
		
		classeur = c;
		
		ModeleFrise modele = new ModeleFrise(classeur);
		
		frise = new JTable(modele);
		ImageIcon [][]valeurTrouve = modele.whereAreValues();
		
		//modele.whereAreValues();
		
		frise.setRowHeight(40);
		
		frise.setDefaultRenderer(Integer.class, new CellRenderer(c,valeurTrouve));
		frise.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		scrollPane = new JScrollPane(frise, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setPreferredSize(new Dimension(1050,200));

		
		this.add(scrollPane);
		for(int i = 0;i <= classeur.getNombreAnnee();i++) {
			frise.getColumnModel().getColumn(i).setPreferredWidth(100);
		}

	}
	
	public void enregistreEcouteur(Controleur parControleur) {
		frise.addMouseListener(parControleur);
	}
	
	public JScrollPane getScrollPaneFrise() {
		return scrollPane;
	}
		

	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
