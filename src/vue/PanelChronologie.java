package vue;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;

import modele.Classeur;
import controleur.Controleur;

public class PanelChronologie extends JPanel {

	
	
	public PanelChronologie(){
//		this.setLayout(new BorderLayout(5,5));
		JPanel panelVide = new JPanel();
		//panelEvenement = new PanelEvenement(classeur);
		this.add(panelVide);
	}
	
	/**
	 * m�thode qui va enlever(removeAll) tout les panels de this puis les ajouter avec leur nouvelle valeur donn�e en param�tre. 
	 * @param PanelEvenement "panelE"
	 * @param PanelFormulaire "pf"
	 * @param Chaine de caract�re (String) "s"
	 */
	
	public void updateDiapo(PanelEvenement panelE,PanelFrise pf,String s) {
		this.removeAll();
		JLabel titreFrise = new JLabel();
		titreFrise.setText("<html>" + s + "<br/></html>");
		titreFrise.setHorizontalAlignment(JLabel.CENTER);
		this.add(titreFrise);
		this.add(panelE);
		this.add(pf);
	}
	
	


	
}
