package vue;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import controleur.Controleur;
import modele.Date;

public class PanelQuestion extends JPanel implements ActionListener {

	
	
	private JTextField fieldIntitules = new JTextField(5);
	
	
	private JTextField fieldDateDebut = new JTextField(5);
	
	private JLabel dateFin = new JLabel("Date (JJ/MM/AAAA)");
	private JTextField fieldDateFin = new JTextField(5);
	
	
	private JComboBox combo1;
	
	private JButton bouton = new JButton("C'est parti");
	
	private JButton chercherFrise = new JButton("Rechercher une frise");
	private final JFileChooser fc = new JFileChooser();
	private String tabPeriode[] = {"1","2","3","4","5","10","20","30","40","50","100"};
	
	public PanelQuestion(){

		this.setLayout(new GridBagLayout());
		
		JLabel intitules = new JLabel("Titre");
		JLabel dateDebut = new JLabel("Date (JJ/MM/AAAA)");
		JLabel periode = new JLabel("Periode");
		GridBagConstraints c = new GridBagConstraints();
		
		
		c.insets = new Insets(5,5,5,5);
		
		JLabel label = new JLabel("Cr�ation d'une frise chronologique ");
		
		c.gridx = 0; c.gridy = 0; c.gridwidth = 3;c.fill = GridBagConstraints.BOTH;
		this.add(label, c);
		
		c.gridx = 0;c.gridy= 1;c.gridwidth = 1;c.gridheight = 1;c.fill = GridBagConstraints.BOTH;
		this.add(intitules,c);
		
		c.gridx++;c.gridwidth = 4;this.add(fieldIntitules,c);
		
		c.gridy++;c.gridx = 0;c.gridwidth = 1;
		this.add(dateDebut,c);
		c.gridx++;c.gridwidth = 4;this.add(fieldDateDebut,c);
		
		c.gridx = 0;c.gridy++;c.gridwidth = 1;
		this.add(dateFin,c);
		c.gridx++;c.gridwidth = 4;this.add(fieldDateFin,c);
		
		c.gridx=0;c.gridy++;this.add(periode,c);
		
		combo1 = new JComboBox(tabPeriode);
		
		c.gridx++;c.gridwidth = 1;this.add(combo1,c);
		
		c.gridy++;c.anchor = GridBagConstraints.CENTER;this.add(bouton,c);
		bouton.setActionCommand("go");

		chercherFrise.setHorizontalAlignment(JButton.CENTER);
		chercherFrise.setVerticalAlignment(JButton.CENTER);
		
		c.gridx = 0; c.gridy++; c.gridwidth = 2;
		this.add(chercherFrise,c);
		chercherFrise.addActionListener(this);
		chercherFrise.setActionCommand("go2");
		fc.setFileFilter(new FileNameExtensionFilter("SER Files","ser"));
		fc.setAcceptAllFileFilterUsed(false);
		
	}
	
	/**
	 * permet de mettre � l'�coute les boutons -> au contr�leur
	 * @param parControleur
	 * 		Controleur "parControleur"
	 */
	
	public void enregistreEcouteur(Controleur parControleur)
	{
		bouton.addActionListener(parControleur);
		chercherFrise.addActionListener(parControleur);
	}
	
	/**
	 * m�thode permettant de convertir une cha�ne de caract�re en Date
	 * 
	 * @param s Une string correspondant au champ fieldDateDebut/fieldDateFin
	 * @return une Date 
	 */
	
	public Date renvoieDate(String s) {
		
		int jour = 0,mois = 0,annee = 0;		
		StringTokenizer st = new StringTokenizer(s,"/");
		jour = Integer.parseInt(st.nextToken());
		mois = Integer.parseInt(st.nextToken());
		annee = Integer.parseInt(st.nextToken());
		
		return new Date(jour,mois,annee);
		
	}

	/**
	 * @return en entier correspondant � la valeur selectionn�e dans la jcombobox 
	 */
	
	public int getPeriode() {
		return getSelectedIntValue(combo1);
	}
	
	/**
	 * 
	 * @return en entier correspondant � l'ann�e de d�but de la frise
	 */
	public int getAnneeDebut() {
		Date dateDebut = renvoieDate(fieldDateDebut.getText());
		int anneeDebut = dateDebut.getAnnee();
		return anneeDebut;
	}
	
	/**
	 * 
	 * @return un entier correspondant � l'ann�e de fin de la frise
	 */
	
	public int getAnneeFin() {
		Date dateFin = renvoieDate(fieldDateFin.getText());
		int anneeFin = dateFin.getAnnee();
		return anneeFin;
	}
	
	/**
	 * 
	 * @return une chaine de caract�re correspondant au titre de la frise
	 */
	
	public String getTitre() {
		return fieldIntitules.getText();
	}
	
	/**
	 * Convertit la valeur selectionn� dans la jcombobox en entier
	 * @param comboBox de type JComboBox
	 * @return un entier 
	 */
	
	public int getSelectedIntValue( JComboBox comboBox ) {
	    return Integer.parseInt((String)comboBox.getSelectedItem());
	}
	
//	public void enregistreEcouteur(Panel p) {
//		bouton.addActionListener(p);
//	}
	
	
	/**
	 * m�thode permettant de convertir tout les champs associ� en une chaine de caract�re de la forme : nom-du-fichier_JJ-MM-AAAA_JJ-MM-AAAA_periode
	 * @return en chaine de caract�re 
	 */
	
	public String convertirChampEnNomFichier() {
		
		String s = new String();
		try {
			StringTokenizer st = new StringTokenizer(fieldIntitules.getText()," ");
			int countToken = st.countTokens();
			int cpt = 1;
			
			while(st.hasMoreTokens()) {
				if(cpt == countToken) {
					s += st.nextToken();
					break;
				}	
				cpt++;
				s += st.nextToken() + "-";
			}
			st = new StringTokenizer(fieldDateDebut.getText(),"/");
			s += "_";
			s += st.nextToken() + "-";
			s += st.nextToken() + "-";
			s += st.nextToken();
			s += "_";
			st = new StringTokenizer(fieldDateFin.getText(),"/");
			s += st.nextToken() + "-";
			s += st.nextToken() + "-";
			s += st.nextToken();
			s += "_";
			s+= getSelectedIntValue(combo1);
		}
		catch(Exception e) {
			System.err.println("Erreur dans le format de la date");
		}
		
		return s;
	}
	
	/**
	 * m�thode qui fait l'inverse de convertirChampEnNomFichier -> elle r�cup�re une chaine de caract�re de la forme nom-de-fichier_JJ-MM-AAAA_JJ-MM-AAAA 
	 * qu'elle va d�couper et replacer au bon endroit dans les champs associ�s 
	 * @param String "s"
	 * Cette m�thode n'est pas utilis�e dans le programme.
	 */
	
	
	public void convertirNomFichierEnChamp(String s) {
		StringTokenizer st = new StringTokenizer(s,"_");
		int cpt = 0;
		String intitule = new String();
		StringTokenizer st2 = new StringTokenizer(st.nextToken(),"-");
		while(st2.hasMoreTokens()) {
			intitule+=st2.nextToken() + " ";
		}
		fieldIntitules.setText(intitule);
		st2 = new StringTokenizer(st.nextToken(),"-");
		while(st2.hasMoreTokens()) {
			fieldDateDebut.setText(st2.nextToken() + "/"+ st2.nextToken()+"/"+st2.nextToken());
		}
		st2 = new StringTokenizer(st.nextToken(),"-");
		while(st2.hasMoreTokens()) {
			fieldDateFin.setText(st2.nextToken() + "/"+ st2.nextToken()+"/"+st2.nextToken());
		}
		st2 = new StringTokenizer(st.nextToken(),".");
		combo1.setSelectedItem(tabPeriode[Integer.parseInt(st2.nextToken()) - 1]);
	}
	
	public void actionPerformed(ActionEvent parEvt) {

		
	}
	
	
	
}