package vue;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

public class ModeleEvenement extends JPanel {

	public ModeleEvenement(JLabel titre, JLabel description, JLabel date, JLabel img) {
		//this.add(img);
//		this.setLayout(new GridBagLayout());
//		GridBagConstraints c = new GridBagConstraints();
//		c.insets = new Insets(5,5,5,5);
		
//		imgDiapo.setLayout(new FlowLayout(FlowLayout.RIGHT));
		
//		diapo.setIcon(img.get(),JLabel.LEFT);
//		c.gridx = 0; c.gridy = 0; c.gridheight = 4; c.gridwidth = 1; c.fill = GridBagConstraints.HORIZONTAL;
		this.add(img);
		img.setHorizontalAlignment(JLabel.CENTER);
		String s = new String("<html>");
		s += "<b><i>"+ date.getText() + "<br/><br/></i>" + titre.getText() + "<br/><br/></b><i>" + description.getText() +"</i></html>";
		JLabel test = new JLabel(s);
		Font f = test.getFont();
		test.setFont(f.deriveFont(f.getStyle() ^ Font.BOLD));
		//test.setHorizontalAlignment(SwingConstants.CENTER);
//		this.add(test);
//		c.gridx = 1; c.gridheight = 1; c.weighty = 1;c.gridwidth = 1; c.gridheight = 1;
		this.add(test);
		test.setHorizontalAlignment(JLabel.CENTER);

	}
	
}
