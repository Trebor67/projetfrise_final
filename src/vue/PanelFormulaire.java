package vue;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import controleur.Controleur;
import modele.Date;
import modele.Evenement;

public class PanelFormulaire extends JPanel implements ActionListener{

	
	private JTextField fieldTitre = new JTextField(5);
	private JTextField fieldDate = new JTextField(5);
	private JTextArea areaDescription = new JTextArea();
	private JComboBox combo1;
	private JButton buttonFind;
	private JButton envoyer = new JButton("Envoyer");
	//private Evenement evt = getEvenement();
	private JLabel imgLabel = new JLabel();
	private final JFileChooser fc = new JFileChooser();
	private GridBagConstraints c = new GridBagConstraints();

	private ImageIcon imageSelectionnee;
	
	/**
	 * 
	 */
	
	public PanelFormulaire() {
		
		this.setLayout(new GridBagLayout());
		JLabel titre = new JLabel("Titre");
		JLabel date = new JLabel("Date (JJ/MM/AAAA)");
		JLabel description = new JLabel("Description");
		JLabel importance = new JLabel("Importance");
		JLabel labelImage = new JLabel("Localisation/chemin de votre image");
		
		ImageIcon searchIcon = new ImageIcon("search.png");
		buttonFind = new JButton(searchIcon);
		
		c.insets = new Insets(6,6,6,6);
		
		c.gridx = 0;c.gridy= 0;c.gridwidth = 1;c.gridheight = 1;c.fill = GridBagConstraints.BOTH;
		this.add(titre,c);
		
		c.gridx++;c.gridwidth = 4;this.add(fieldTitre, c);
		
		c.gridwidth = 1;c.gridx = 0;c.gridy++; 
		this.add(date,c);
		c.gridx++; c.gridwidth = 4; this.add(fieldDate,c);
		
		c.gridx = 0;c.gridy++;c.gridwidth = 1;this.add(importance,c);
		String tabImportance[] = {"1","2","3","4"};
		combo1 = new JComboBox(tabImportance);
		c.gridx++;c.gridwidth = 3;this.add(combo1,c);
		
		
		c.gridx = 0; c.gridy++; c.gridwidth = 1;
		this.add(labelImage, c);
		
		c.gridx++; c.gridwidth = 1;
		c.fill = GridBagConstraints.NONE;
		this.add(buttonFind, c);
		buttonFind.setFocusPainted(false);
		buttonFind.setBorderPainted(false);
		buttonFind.setBackground(Color.WHITE);
		buttonFind.setActionCommand("find");
		buttonFind.addActionListener(this);
		
		c.gridx = 0; c.gridy++; c.gridwidth = 1; c.fill = GridBagConstraints.NONE;c.gridwidth = 1;
		c.anchor = GridBagConstraints.NORTHWEST;
		this.add(description, c);
		
		c.fill = GridBagConstraints.BOTH;c.gridx++; c.anchor = GridBagConstraints.CENTER; c.gridwidth = 4;
		
		areaDescription.setSize(new Dimension(100,100));
		areaDescription.setPreferredSize(new Dimension(300,200));
		areaDescription.setWrapStyleWord(true);
		areaDescription.setLineWrap(true);
		
		this.add(areaDescription, c);
		
		
		
		c.gridx = 1; c.gridy++; c.gridwidth = 3;
		this.add(envoyer,c);
		envoyer.setActionCommand("envoyer");
		
		c.gridx = 1; c.gridy++;  c.gridheight = 1; c.gridwidth = 1;
		
		
	}
	
	/** 
	 * �a sert � faire un enregistre �couteur , vous avez des questions ? mdr xD
	 * @param parControleur ouais la javadoc !!!!!!!!!!!!
	 * 
	 */
	
	public void enregistreEcouteur(Controleur parControleur) {
		envoyer.addActionListener(parControleur);
	}
	
	/** 
	 * Methode permettant de convertir une string en Date
	 */
	public Date renvoieDate(String s) {
		
		int jour = 0,mois = 0,annee = 0;		
		StringTokenizer st = new StringTokenizer(s,"/");
		jour = Integer.parseInt(st.nextToken());
		mois = Integer.parseInt(st.nextToken());
		annee = Integer.parseInt(st.nextToken());
		
		return new Date(jour,mois,annee);
		
	}
	
	
	/**
	 * 
	 * @return Un evenement de type Evenement en r�cup�rant toutes les valeurs saisies dans les champs 
	 */
	
	public Evenement getEvenement() {
		int a = Integer.parseInt(this.combo1.getSelectedItem().toString());
		return new Evenement(renvoieDate(fieldDate.getText()), fieldTitre.getText(), areaDescription.getText(),a,imageSelectionnee);
		
	}

	/**
	 * 
	 * ActionPerformed local pour selectionner une image, pas besoin de le mettre dans le controleur 
	 * 
	 */
	public void actionPerformed(ActionEvent evt) {
		if(evt.getActionCommand().equals("find")) {
			fc.setFileFilter(new FileNameExtensionFilter("Image files", ImageIO.getReaderFileSuffixes()));
			fc.setAcceptAllFileFilterUsed(false);
			int returnValue = fc.showOpenDialog(null);
			if(returnValue == JFileChooser.APPROVE_OPTION) {
				File f = fc.getSelectedFile();
				System.out.println(f.getName());
				ImageIcon icon = new ImageIcon(f.getPath()); 
				Image img = icon.getImage(); 
				BufferedImage bi = new BufferedImage(200, 120, BufferedImage.TYPE_INT_ARGB); 
				Graphics g = bi.createGraphics(); 
				g.drawImage(img, 0, 0, 200, 120, null); 
				imageSelectionnee = new ImageIcon(bi);
				// ImageIcon image = new ImageIcon(evenement.getImage().getImage().getScaledInstance(65, 65, Image.SCALE_DEFAULT));
				this.add(imgLabel,c);
				imgLabel.setIcon(imageSelectionnee);
				imgLabel.setPreferredSize(new Dimension(200,120));
				//imgLabel.
				//this.updateUI();
			}
		}
	}
	
	/**
	 * remet les champs � 0
	 */
	
	public void clear() {
		fieldTitre.setText(new String());
		areaDescription.setText(new String());
		fieldDate.setText(new String());
	}
	
	
//	M�thode non utlis�e 
//	
//	public int getImportance() {
//		return getSelectedIntValue(combo1);
//	}
//	
	
	/**
	 * M�thode qui convertir la valeur selectionn�e dans la combobox en entier
	 * @param comboBox de type JComboBox
	 * @return un entier
	 */
	public int getSelectedIntValue( JComboBox comboBox ) {
	    return Integer.parseInt((String)comboBox.getSelectedItem());
	 }
}
