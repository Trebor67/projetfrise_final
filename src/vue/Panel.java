package vue;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JPanel;

import modele.Classeur;
import modele.Date;
import modele.Evenement;
import controleur.Controleur;
import controleur.Fichier;
//
public class Panel extends JPanel implements ActionListener{
	
	
	/**
	 * CardLayout du JPanel
	 */
	
	private CardLayout chCardLayout = new CardLayout();
	
	/**
	 * La classe Panel va contenir les 3 panels principaux du programme : 
	 * - PanelQuestion permmetant de cr�er une frise
	 * - PanelFormulaire permettant d'ajouter un �v�nement � la frise
	 * - PanelChronologie panel principal du programme qui permet l'affichage du diaporama d'�v�nements et de la JTable
	 * 
	 * Cette classe � pour Layout un CardLayout permettant de naviguer entre les diff�rents panels gr�ce 
	 */
	
	public Panel() {
		
		this.setLayout(chCardLayout);
		PanelQuestion pq = new PanelQuestion();
		PanelFormulaire pf = new PanelFormulaire();
		this.setBackground(new Color(255,0,0));
		this.setForeground(new Color(255,0,0));
		this.setOpaque(true);
		this.setVisible(true);
		Classeur classeur = new Classeur();
		
//		classeur.ajout(new Evenement(new Date(23,05,2000),"titre mdr","description de merde lol",3,new ImageIcon("sign-check-icon.png")));
//		classeur.ajout(new Evenement(new Date(14,05,2005),"mdr","Super la descript ! Super la descript ! Super la descript ! Super la descript ! Super la descript ! Super la descript ! Super la descript ! Super la descript ! Super la descript ! Super la descript ! Super la descript ! Super la descript ! Super la descript ! Super la descript ! Super la descript !",2,new ImageIcon("sign-check-icon.png")));
//		classeur.ajout(new Evenement(new Date(23,05,2004),"titre ","description ",4,new ImageIcon("sign-check-icon.png")));
		PanelChronologie pc = new PanelChronologie();
		Controleur c = new Controleur(pf,pq,pc,classeur);
		this.add(pc);
		
		this.add(pq,"pq");
		this.add(pf);
		
		//pq.setBackground(new Color(255,0,0));
		chCardLayout.addLayoutComponent(pc, "Chronologie");
		chCardLayout.addLayoutComponent(pq, "Frise");
		chCardLayout.addLayoutComponent(pf, "Evenement");
		chCardLayout.show(this,"pq");
		
	}

	/**
	 * methode permettant de naviguer entre les panels
	 */
	public void actionPerformed(ActionEvent parEvt) {
		if(parEvt.getActionCommand().equals("Cr�ation Frise")) {
			chCardLayout.show(this, "Frise");
		}
		else if(parEvt.getActionCommand().equals("Ajout Evenement")) {
			chCardLayout.show(this, "Evenement");
		}
		else if(parEvt.getActionCommand().equals("Affichage")) {
			chCardLayout.show(this, "Chronologie");
		}

	}
	
	public CardLayout returnCard() {
		return chCardLayout;
	}
}