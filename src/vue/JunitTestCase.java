package vue;

import static org.junit.Assert.*;

import org.junit.Test;

import modele.Classeur;
import modele.Date;
import modele.Evenement;
import vue.PanelChronologie;

public class JunitTestCase {

	@Test
//	public void testCompareTo() {
//		
//		Date d1 = new Date(12,04,1998);
//		Date d2 = new Date(12,04,1998);
//		
//		if(d1.compareTo(d2) == 0)
//			fail("m�me date");
//		else if(d1.compareTo(d2) > 0)
//			fail("this est sup�rieur au param�tre");
//		else if(d1.compareTo(d2) < 0)
//			fail("this est inf�rieur au param�tre");
//		
//	}
	
	public void testAjoutEvt() {
		Classeur c = new Classeur();
		Evenement e1 = new Evenement(new Date(12,04,1998),"titre","description",2);
		try {
			c.ajout(e1);
		}
		catch(Exception e) {
			fail("Il y a eu une erreur dans l'ajout de l'�v�nement");
		}
		fail("Evenement ajout� avec succ�s");
		
	}
	
	@Test
	public void testSetTitre() {
		String s = new String("test");
		Classeur c = new Classeur();
		c.setTitre(s);
		assertEquals(c.getTitreFrise(),s);
	}
	@Test
	public void testSetAnneeDebut() {
		int a = 40;
		Classeur c = new Classeur();
		c.setAnneeDebut(a);
		assertEquals(c.getAnneeDebut(),a);
	}
	@Test
	public void testSetAnneeFin() {
		int a = 40;
		Classeur c = new Classeur();
		c.setAnneeFin(a);
		assertEquals(c.getAnneeFin(),a);
	}
	@Test
	public void testSetPeriode() {
		int a = 5;
		Classeur c = new Classeur();
		c.setPeriode(a);
		assertEquals(c.getPeriode(),a);
	}
	@Test
	public void testGetTitre() {
		String s = new String("titre");
		Classeur c = new Classeur();
		c.setTitre("titre");
		assertEquals(s,c.getTitreFrise());
	}
	@Test
	public void testGetAnneeDebut() {
		int a = 1998;
		Classeur c = new Classeur();
		c.setAnneeDebut(1998);
		assertEquals(a,c.getAnneeDebut());
	}
	@Test
	public void testGetAnneeFin() {
		int a = 1998;
		Classeur c = new Classeur();
		c.setAnneeDebut(1998);
		assertEquals(a,c.getAnneeDebut());
	}
	@Test
	public void testGetPeriode() {
		int a = 5;
		Classeur c = new Classeur();
		c.setPeriode(5);
		assertEquals(a,c.getPeriode());
	}
	@Test
	public void testGetNombreAnnee() {
		Classeur c = new Classeur();
		c.setAnneeDebut(1998);
		c.setAnneeFin(2000);
		int a = 2;
		assertEquals(a,c.getNombreAnnee());
	}
	@Test
	public void testGetTitreEvenement() {
		String s = new String("titre");
		Evenement e = new Evenement(new Date(12,04,1998),"titre","description",2);
		assertEquals(s,e.getTitre());
	}
	@Test
	public void testGetDescriptionEvenement() {
		String s = new String("description");
		Evenement e = new Evenement(new Date(12,04,1998),"titre","description",2);
		assertEquals(s,e.getDescription());
	}
	@Test
	public void testGetImportance() {
		int a = 5;
		Evenement e = new Evenement(new Date(12,04,1998),"titre","description",5);
		assertEquals(a,e.getImportance());
	}

}
