package vue;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextPane;

import controleur.Controleur;
import modele.Classeur;
import modele.Date;
import modele.Evenement;

public class PanelEvenement extends JPanel implements ActionListener{
	
	private CardLayout chCardLayout = new CardLayout();
	
	
	private JButton bouton[] = new JButton[2];
	
	private JLabel labelDate[];
	private JPanel panelCentre = new JPanel();
	
	public PanelEvenement(Classeur c) {
		
		String intitule_bouton[] = {"<",">"};
		for(int i = 0 ; i < 2 ; i++) {
			bouton[i] = new JButton(intitule_bouton[i]);
			bouton[i].addActionListener(this);
		}
//		this.setLayout(new BorderLayout(5,5));
		this.add(bouton[0]);
		this.add(panelCentre);
		panelCentre.setLayout(chCardLayout);
		
		
		
		
		
		int cpt = c.nbElement();
		JLabel [] labelTitre = new JLabel[cpt];
		labelDate = new JLabel[cpt];
		JLabel [] labelImage = new JLabel[cpt];
		JLabel [] areaDescription = new JLabel[cpt];
		ModeleEvenement [] panelDiapo = new ModeleEvenement[cpt];
		
		cpt = 0;
		TreeMap <Integer,TreeSet<Evenement>> hashMap = c.getClasseur();
		for(Integer i : hashMap.keySet()) {
			for(Evenement e : hashMap.get(i) ) {
				
				labelTitre[cpt] = new JLabel(e.getTitre());
				areaDescription[cpt] = new JLabel(stringToHTML(e.getDescription()));
				labelDate[cpt] = new JLabel(e.getDate().toString3());
				labelImage[cpt] = new JLabel(e.getImage());
				
				
				panelDiapo[cpt] = new ModeleEvenement(labelTitre[cpt],areaDescription[cpt],labelDate[cpt],labelImage[cpt]);
				
				panelCentre.add(panelDiapo[cpt],String.valueOf(cpt));
				
				cpt++;
			}
		}
		
		
		this.add(bouton[1]);
		bouton[0].setActionCommand("bouton1");
		bouton[1].setActionCommand("bouton2");
	}
	
	
	/**
	 * M�thode qui convertit la description de l'�v�nement en texte HTML et fait en retour � la ligne tout les 18 mots, et rajoute (...) si la description 
	 * fait + de 4 lignes
	 * @param String "s"
	 * @return une chaine de caract�re format�e en language HTML
	 */
	public String stringToHTML(String s) {
		String stringHTML = new String();
		stringHTML += "<html>";
		StringTokenizer st = new StringTokenizer(s);
		int i = 0;
		int cpt = 0;
		while(st.hasMoreTokens()) {
			
			
			if(i%22 == 21) { // Tout les 18 mots on ins�re un saut de ligne
				stringHTML += "<br/>";
				cpt++;
			}
			if(cpt == 4) {
				stringHTML += " (...)";
				break;
			}
			stringHTML += " " + st.nextToken();
			i++;
			
		}
		stringHTML += "</html>";
		return stringHTML;
	}
	
	
	/**
	 * met � l'�coute les deux boutons (gauche, droite) du diaporama au controleur 
	 * @param Controleur "parControleur"
	 */
	
	public void enregistreEcouteur(Controleur parControleur) {
		bouton[0].addActionListener(parControleur);
		bouton[1].addActionListener(parControleur);
	}
	
//	public JPanel getPanelCentre() {
//		return panelCentre;
//	}
	
	
	/**
	 * 
	 * @param entier "i"
	 * @return retourne une chaine de caract�re correspondant au champ/JTextField "fieldDate"
	 */
	public String getDate(int i) {
		return labelDate[i].getText();
	}
	
	/**
	 * affiche le diapo au label correspondant 
	 * @param Chaine de caract�re "s"
	 */
	
	public void synchroDiapoAvecJTable(String s) {
		chCardLayout.show(panelCentre, s);
		//panelDiapo[0]
	}
	
	public void actionPerformed(ActionEvent parEvt) {
		if(parEvt.getSource() == bouton[0]) 
			chCardLayout.previous(panelCentre);
		else
			chCardLayout.next(panelCentre);
	}
	

}
