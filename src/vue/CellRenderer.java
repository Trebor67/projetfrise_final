package vue;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.util.HashMap;
import java.util.TreeSet;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import modele.Classeur;
import modele.Evenement;

public class CellRenderer extends JLabel implements TableCellRenderer{

	private Classeur classeur;
	private ImageIcon[][] valTrouve;
	
	public CellRenderer(Classeur c, ImageIcon[][] val) {
		super();
		classeur = c;
		valTrouve = val;
		setOpaque(true);
		setHorizontalAlignment(JLabel.CENTER);
		this.setForeground(new  java.awt.Color(180,100,40));
	}
	

	/**
	 * ins�re une image dans la jtable si la valeurTrouve est diff�rent de null 
	 */
	
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean isFocus, int row, int col) {
		// TODO Auto-generated method stub
		//setBackground(Color.GRAY);
		setBackground(Color.WHITE);
		//System.out.println(valTrouve[row][col]);
		
		if(col < classeur.getNombreAnnee() + 1 && valTrouve[row][col] != null) {
			this.setIcon(valTrouve[row][col]);
		}
		else {
			this.setIcon(null);
		}
		
		return this;
	}

}
