package modele;

import java.util.GregorianCalendar;
import java.io.Serializable;
import java.util.Calendar;
 
public class Date implements Comparable <Date>, Serializable {
  private int chJour;
  private int chMois;
  private int chAnnee;
  private int chJourSemaine ;  
   
  public Date ()   { 
	  GregorianCalendar dateAuj = new GregorianCalendar ();
	  chAnnee = dateAuj.get (Calendar.YEAR);
	  chMois = dateAuj.get (Calendar.MONTH)+1; // janvier = 0, fevrier = 1...
	  chJour = dateAuj.get (Calendar.DAY_OF_MONTH);
	  chJourSemaine = dateAuj.get (Calendar.DAY_OF_WEEK);
  }
  
  public int getWeekOfYear() {
	  GregorianCalendar date = new GregorianCalendar (chAnnee,chMois-1,chJour);
	  int weekOfyear = date.get (Calendar.WEEK_OF_YEAR);				
	  
	  return weekOfyear;
  }
  
  
  public Date (int parchJour, int parchMois, int parchAnnee)   {   
	chJour = parchJour;
	chMois = parchMois;
	chAnnee = parchAnnee; 
	GregorianCalendar date = new GregorianCalendar (chAnnee,chMois-1,chJour);
	chJourSemaine = date.get (Calendar.DAY_OF_WEEK);				
  } 
   
  /**
   * retourne 0 si this et parDate sont �gales, 
   * -1 si this pr�c�de parDate,
   *  1 si parDate pr�c�de this
   */
  public int compareTo (Date parDate) {
    if (chAnnee < parDate.chAnnee)
		return -1;
	if (chAnnee > parDate.chAnnee)
		return 1;
	// les ann�es sont =
	if (chMois < parDate.chMois)
		return -1;
	if (chMois > parDate.chMois)
		return 1;
	// les chMois sont =
	if (chJour < parDate.chJour)
		return -1;
	if (chJour > parDate.chJour)
		return 1;
	return 0;	
  }
 
  public Date dateDuLendemain ()   {	
    if (chJour < dernierchJourDuchMois(chMois,chAnnee))
		     return  new Date (chJour+1,chMois,chAnnee);
		else if (chMois < 12)
				return new Date (1,chMois+1,chAnnee);
			 else return new Date (1,1,chAnnee+1);	
  }  
  
  public Date dateDeLaVeille () {    
	if (chJour > 1)
			return  new Date (chJour-1,chMois,chAnnee);
	else if (chMois > 1)
			   return new Date (Date.dernierchJourDuchMois(chMois-1, chAnnee),chMois-1,chAnnee);
		 else return  new Date (31,12,chAnnee-1);
  }	 
  
  public static int dernierchJourDuchMois (int parchMois, int parchAnnee) {
		switch (parchMois) {
			 case 2 : if (estBissextile (parchAnnee))  return 29 ; else return 28 ;  
			 case 4 : 	 case 6 : 	 case 9 : 	 case 11 : return 30 ;
			 default : return 31 ;
			}  // switch
	  } 
	  
  private static boolean estBissextile(int parchAnnee) {
			return parchAnnee % 4 == 0 && (parchAnnee % 100 != 0 || parchAnnee % 400 == 0);
	  }
    
  public String toString(){
	String []strchJour = {"dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi"};
	String []strchMois = {"janvier","fevrier","mars","avril","mai","juin","juillet","aout","septembre","octobre","novembre","decembre"};
	
	return strchJour[chJourSemaine - 1] + " " + chJour + " " + strchMois[chMois - 1] + " " + chAnnee;		 
}

  public String toString3() {
	  return chJour + "/" + chMois + "/" + chAnnee;
  }
  
  public String toString2() {
	
	String []strJour = {"dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi"};
	String []strMois = {"janvier","fevrier","mars","avril","mai","juin","juillet","aout","septembre","octobre","novembre","decembre"};
	
	return strJour[chJourSemaine - 1] + " " + chJour + " " + strMois[chMois - 1] + " " + chAnnee;
			//+ " " + chHeure + "h" + chMinute;		 
}

  public boolean estValide() {
	  return chJour > 0 && chJour <= 12 && chMois > 0 && chMois <= 12 && chAnnee > 0;
  }

public int getAnnee() { 
	return chAnnee;
}

public int getJour() { 
	return chJour;
}

public int getMois() { 
	return chMois;
}

public int getJourSemaine () {
	return chJourSemaine;
}

/**
 * retourne la date du premier chJour de la semaine contenant this 
 * @return
 */
public Date datePremierchJourSemaine () {
	Date datePrem = this;	 
	while (datePrem.getJourSemaine()!=2) {
		datePrem = datePrem.dateDeLaVeille();
	}
	return datePrem;
}

public boolean isToday() {
	return new Date().compareTo(this) == 0;
}
}  // class Date
