package modele;

import java.io.Serializable;
import java.util.StringTokenizer;

import javax.swing.ImageIcon;

public class Evenement implements Comparable<Evenement>, Serializable {
	private Date chDate = new Date();
	private String chTitre;
	private String chDescription;
	private ImageIcon chImage;
	private int importance;
	private int numeroEvenement = -1;
	
	public Evenement(Date parDate, String parTitre, String parDescription,int imp, ImageIcon img)
	{
		
		chDate = parDate;
		chTitre = parTitre;
		chDescription = parDescription;
		chImage = img;
		importance = imp;
		numeroEvenement++;
		
	}
	public Evenement(Date parDate, String parTitre, String parDescription,int imp)
	{
		chDate = parDate;
		chTitre = parTitre;
		chDescription = parDescription;
		importance = imp;
		numeroEvenement++;
	}
	
	
	
	public String toString()
	{
		String res = chTitre + " le " + chDate + "importance : --> "+ importance+ "\n" + chDescription;
		return res;
	}
	
	/**
	 * 
	 * @return La date correspondante sous forme d'une classe Date
	 */
	
	public Date getDate(){
		return this.chDate;
	}
	
//	public int getNumeroEvt() {
//		return numeroEvenement;
//	}
	
//	public int compareTo(Evenement evt) {
//		int retour = this.chDate.compareTo(evt.chDate);
//		return retour;
//	}
//	
//
//	public int compareTo(Evenement evt) {
//		if(chDate.compareTo(evt.chDate) < 0)
//			return -1;
//		else if(chDate.compareTo(evt.chDate) > 0)
//			return 1;
//		else
//			return 0;
//	}
	
	/**
	 * @param Evenement
	 * 			Un �v�nement "evt"
	 * @return un entier si this est plus petit, plus grand ou �gal au param�tre
	 */
	
	public int compareTo(Evenement evt) {
		if(this.importance > evt.importance)
			return -1;
		else if(this.importance < evt.importance)
			return 1;
		else
			return 0;
	}
	
	
	/**
	 * @return Le titre correspondant sous forme de String
	 */
	
	public String getTitre() {
		return chTitre;
	}
	
	/**
	 * 
	 * @return La description correspondante sous forme de String
	 */
	
	public String getDescription() {
		return chDescription;
	}
	
	/**
	 * 
	 * @return L'image correspondante sous forme ImageIcon
	 */
	
	public ImageIcon getImage() {
		return chImage;
	}
	
	/**
	 * 
	 * @return L'importance sous forme d'entier
	 */
	
	public int getImportance(){
		return importance;
	}
}
