package modele;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

public class Classeur implements Serializable {
	
	private String titreFrise;
	private int anneeDebut;
	private int anneeFin;
	private int periode;
	private TreeMap<Integer, TreeSet<Evenement>> treeChrono;
	
	
	public Classeur(){
		treeChrono = new TreeMap<Integer, TreeSet<Evenement>>();
		titreFrise = "";
		anneeDebut = 0;
		anneeFin = 0;
		periode = 0;
	}
	
	public Classeur(String titre, int anneeD, int anneeF, int p) {
		titreFrise = titre;
		anneeDebut = anneeD;
		anneeFin = anneeF;
		periode = p;
		treeChrono = new TreeMap<Integer, TreeSet<Evenement>>();
	}
	
	
	/**
	 * 
	 * Ajoute dans la treemap l'evenement donn� en param�tre. Si l'ann�e d'�v�nement existe, il le rajoute dans le TreeSet, sinon il cr�e un nouveau TreeSet
	 * 
	 * @param evt
	 * 			Un �v�nement "evt"
	 * @return void
	 */
	
	public void ajout(Evenement evt){
		if (treeChrono.containsKey(evt.getDate().getAnnee())){
			TreeSet<Evenement> tmpTree = treeChrono.get(evt.getDate().getAnnee());
			
			tmpTree.add(evt);
			treeChrono.put(evt.getDate().getAnnee(),tmpTree);
		}
		else{
			TreeSet<Evenement> tmpTree = new TreeSet<Evenement>();
			tmpTree.add(evt);
			treeChrono.put(evt.getDate().getAnnee(),tmpTree);
		}
	}
	
	/**
	 * 
	 * m�thode setter permettant der modifier le champ priv� "titreFrise"
	 * 
	 * @param s
	 * 		Une String "s"
	 * 
	 * @return void
	 */
	
	public void setTitre(String s) {
		titreFrise = s;
	}
	
	
	/**
	 * m�thode setter permettant de modifier le champ priv� "anneeDebut"
	 * 
	 * @param annee
	 * 		Un entier "ann�e"
	 */
	public void setAnneeDebut(int annee) {
		anneeDebut = annee;
	}
	
	/**
	 * m�thode setter permettant de modifier le champ priv� "anneeFin"
	 * 
	 * @param annee
	 * 		Un entier "ann�e"
	 * @return void
	 */
	
	public void setAnneeFin(int annee) {
		anneeFin = annee;
	}
	
	/**
	 * m�thode setter permettant de modifier le champ priv� "periode"
	 * @param p
	 * 		Un entier "p"
	 * @return void
	 */
	
	public void setPeriode(int p) {
		periode = p;
	}
	
	/**
	 * methode getter permettant de r�cup�rer le champ priv� "titreFrise"
	 * @return une String correspondant � "titreFrise"
	 */
	
	public String getTitreFrise() {
		return titreFrise;
	}
	
	/**
	 * methode getter permettant de r�cup�rer le champ priv� "anneeDebut"
	 * @return un entier, correspondant � "anneeDebut"
	 */
	public int getAnneeDebut() {
		return anneeDebut;
	}
	
	/**
	 * methode getter permettant de r�cup�rer le champ priv� "anneeDebut"
	 * @return un entier, correspondant � "anneeFin"
	 */
	
	public int getAnneeFin() {
		return anneeFin;
	}
	
	/**
	 * methode getter permettant de r�cup�rer le champ priv� "periode"
	 * @return en entier, correspondant � "periode"
	 */
	
	public int getPeriode() {
		return periode;
	}
	
	/**
	 * methode getter permettant de r�cup�rer le nombre d'ann�e en faisant la diff�rence entre anneeFin - anneeDebut
	 * @return un entier correspondant � anneeFin - anneeFin
	 */
	
	public int getNombreAnnee() {
		return anneeFin - anneeDebut;
	}
	
	/**
	 * Compte le nombre d'�l�ments contenu dans la TreeMap
	 * @return un entier correspondant au nombre d'�lements contenu dans la TreeMap
	 */
	
	public int nbElement() {
		int cpt = 0;
		for(Integer i : treeChrono.keySet()) {
			for(Evenement e : treeChrono.get(i)) {
				cpt++;
			}
		}
		return cpt;
	}
	
	
	public String toString(){
		return treeChrono.toString();
	}
	
	/**
	 * m�thode getter permettant de r�cuperer la TreeMap 
	 * @return une TreeMap correspondant � "treeChrono"
	 */
	
	public TreeMap<Integer, TreeSet<Evenement>> getClasseur() {
		return treeChrono;
	}
}
