package controleur;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;

import modele.Classeur;
import modele.Date;
import modele.Evenement;
import vue.FenetreMere;
import vue.ModeleEvenement;
import vue.ModeleFrise;
import vue.Panel;
import vue.PanelChronologie;
import vue.PanelEvenement;
import vue.PanelFormulaire;
import vue.PanelFrise;
import vue.PanelQuestion;

public class Controleur implements ActionListener,MouseListener{//

	private PanelFormulaire panelFormulaire;
	private Classeur classeur;
	private PanelEvenement panelEvenement;
	private PanelQuestion panelQuestion;
	private PanelChronologie panelChronologie;
	private PanelFrise panelFrise;
	private int compteur = 0;
	private String nomFichier;
	
	public Controleur(PanelFormulaire pf,PanelQuestion pq, PanelChronologie pc, Classeur c) {
		panelFormulaire = pf;
		classeur = c;
		panelQuestion = pq;
		panelChronologie = pc;
		panelFormulaire.enregistreEcouteur(this);
		panelQuestion.enregistreEcouteur(this);
	}
	
	
	/**
	 * methode qui permet de g�rer les clic sur l'interface graphique. Selon le clic effectu� l'utilisateur va cr�er une frise, charger une frise,
	 * ajouter un �v�nement � la frise, naviguer entre les �v�nements tout en synchronisant la jscrollbar  
	 * @param un ActionEvent "evt"
	 */
	
	public void actionPerformed(ActionEvent evt) {
		
		if(evt.getActionCommand().equals("envoyer")) {
			
			Evenement evt2 = panelFormulaire.getEvenement();
			classeur.ajout(evt2);
			Fichier.ecrireFichierClasseur(classeur, nomFichier);
			panelEvenement = new PanelEvenement(classeur);
			panelFrise = new PanelFrise(classeur);
			panelEvenement.enregistreEcouteur(this);
			panelFrise.enregistreEcouteur(this);
			
			panelChronologie.updateDiapo(panelEvenement,panelFrise,classeur.getTitreFrise());
			
			this.synchroClicAvecDiapo(compteur);
			
			panelFormulaire.clear();
			
		}
		else if(evt.getActionCommand().equals("go")){
			
			classeur = new Classeur(panelQuestion.getTitre(),panelQuestion.getAnneeDebut(),panelQuestion.getAnneeFin(),panelQuestion.getPeriode());
			nomFichier = panelQuestion.convertirChampEnNomFichier()+".ser";
			panelEvenement = new PanelEvenement(classeur);
			panelEvenement.enregistreEcouteur(this);
			panelFrise = new PanelFrise(classeur);
			panelFrise.enregistreEcouteur(this);
			System.out.println(panelQuestion.convertirChampEnNomFichier());
			panelChronologie.updateDiapo(panelEvenement,panelFrise,classeur.getTitreFrise());
			
		}
		else if(evt.getActionCommand().equals("go2")) {
			JFileChooser fc = new JFileChooser();
			fc.setFileFilter(new FileNameExtensionFilter("SER Files","ser"));
			fc.setAcceptAllFileFilterUsed(false);
			int returnValue = fc.showOpenDialog(null);
			if(returnValue == JFileChooser.APPROVE_OPTION) {
				File f = fc.getSelectedFile();
				System.out.println(f.getName());
				classeur =Fichier.lireFichierClasseur(f.getName());
				nomFichier = f.getName();
			}
			
			panelEvenement = new PanelEvenement(classeur);
			panelEvenement.enregistreEcouteur(this);
			panelFrise = new PanelFrise(classeur);
			panelFrise.enregistreEcouteur(this);
			panelChronologie.updateDiapo(panelEvenement,panelFrise,classeur.getTitreFrise());
		}
		else if(evt.getActionCommand().equals("bouton1")) {
			compteur--;
			if(compteur < 0) {
				compteur = classeur.nbElement() - 1;
			}
			//System.out.println(panelEvenement.getDate(compteur));
			// 2008 - 2000 = 8 ; on sait qu'on � la 8eme colonne, 
			// Point p ; p.setX(8*100);
			Point p = new Point();
			Date d = panelFormulaire.renvoieDate(panelEvenement.getDate(compteur)); // CONVERTIT LA STRING (DATE) DU DIAPO COURANT CONTENU DANS UN JLABEL -> EN DATE
			int annee = d.getAnnee();
			int x = annee - classeur.getAnneeDebut();
			p.setLocation((x * 100), 1);
			panelFrise.getScrollPaneFrise().getViewport().setViewPosition(p);
			
		}
		else if(evt.getActionCommand().equals("bouton2")) {
			compteur++;
			if(compteur >= classeur.nbElement()) {
				compteur = 0;
			}
			
			Point p = new Point();
			Date d = panelFormulaire.renvoieDate(panelEvenement.getDate(compteur)); // CONVERTIT LA STRING (DATE) DU DIAPO COURANT CONTENU DANS UN JLABEL -> EN DATE
			
			int annee = d.getAnnee();
			int x = annee - classeur.getAnneeDebut();
			
			p.setLocation((x * 100), 1);
			
			panelFrise.getScrollPaneFrise().getViewport().setViewPosition(p);
		}
	}

	/**
	 * methode permettant de faire appel � la m�thode synchroDiapoAvecJTable de la classe PanelEvenement
	 * @param numero
	 * 		entier "numero" correspondant au num�ro d'�venement dans la TreeMap
	 */
	
	public void synchroClicAvecDiapo (int numero) {
		
		panelEvenement.synchroDiapoAvecJTable(String.valueOf(numero));
	}
	
	/**
	 * methode permettant de r�cup�rer le clic souris dans la jtable et va synchroniser la jscrollbar � l'endroit ou le clic a �t� effectu� ainsi que le
	 * diaporama en faisant appel � la methode synchroClicAvecDiapo 
	 * @param un MouseEvent "arg0"
	 */
	
	public void mouseClicked(MouseEvent arg0) {
		
		JTable table =(JTable)arg0.getSource();
		ModeleFrise model = (ModeleFrise)table.getModel();
		Point point = arg0.getPoint();
		int rowIndex = table.rowAtPoint(point);
		int colIndex = table.columnAtPoint(point);
		
		System.out.println(point.getX() + " " + point.getY() );
		
		Point nouveauPoint = point.getLocation();
		
		nouveauPoint.setLocation(((int)point.getX() / 100) * 100, point.getY()); // on convertit l'axe des abscisses (double) en int, on divise et on remultiplie par 100 pour afficher au bon endroit
		
		panelFrise.getScrollPaneFrise().getViewport().setViewPosition(nouveauPoint);
		
		if(model.getValueAt(rowIndex, colIndex) != null ){
			Evenement ev = (Evenement) model.getValueAt(rowIndex,colIndex);
			
			int importance = ev.getImportance(), annee = ev.getDate().getAnnee() , cpt = 0;
			
			TreeMap <Integer,TreeSet<Evenement>> hashMap = classeur.getClasseur();
			for(Integer i : hashMap.keySet()) {
				for(Evenement e : hashMap.get(i) ) {
					
					if(e.getDate().getAnnee() == annee && e.getImportance() == importance ) {
						this.synchroClicAvecDiapo(cpt);
						compteur = cpt;
					}
					else {
						cpt++;
					}
				}
				
			}
		}
	}

	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}