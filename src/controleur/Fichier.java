package controleur;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JOptionPane;

import modele.Classeur;

public class Fichier {

	public static Classeur lireFichierClasseur(String nomFichier) {
		
		ObjectInputStream ois;
		Classeur Classeur = new Classeur();
		File f = new File(nomFichier);
		if(f.exists()) {
			try {
				ois = new ObjectInputStream ( 
						new BufferedInputStream ( 
							new FileInputStream(
								f)));
			Classeur = (Classeur) ois.readObject();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				System.err.println("Erreur : tentative de lecture � un fichier vide... \nCr�ation d'un nouveau Classeur reussi avec succ�s ! ");
			}
			
		}
		else {
			try {
				f.createNewFile();
				Classeur = new Classeur();
			} catch (IOException e1) {
				System.err.println("Erreur survenu pendant cr�ation fichier");
			}
		}
		return Classeur;
	}
	
	public static void ecrireFichierClasseur(Classeur a, String nomFichier) {
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(
					new BufferedOutputStream(
						new FileOutputStream(
							new File(nomFichier))));
		
		oos.writeObject(a);
		oos.close();
		JOptionPane.showMessageDialog(null, "Classeur sauvegard� avec succ�s !");
		
		}    	
		catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "une erreur est survenu pendant la sauvegarde de votre Classeur");
	    }     	
		
	}
}
